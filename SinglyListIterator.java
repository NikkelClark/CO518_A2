import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Write a description of class SinglyListIterator here.
 *
 * @author Ryan Clark
 * @version 30 November 2017
 */
public class SinglyListIterator<E> implements ListIterator<E>
{
    // instance variables - replace the example below with your own
    private SinglyLinkedList object;
    private Node<E> currentNode;
    private Node<E> previousNode;
    private int cursor;
    private int size;

    /**
     * Constructor for objects of class SinglyListIterator
     * Object contains an instance of SinglyLinkedList
     * curretNode hold the next() node therefore the next method will return it
     * previousNode hold the previous element used mainly for the add method
     * cursor hold the position of the currentNode/next() starts from 0 
     * (EG: cursor = 3 currentnode = E4 previous = E3)
     * size is the overall size of the list.
     */
    public SinglyListIterator(int pos, SinglyLinkedList obj)
    {
        // initialise instance variables
        this.object = obj;
        currentNode = object.startNode;
        previousNode = null;
        size = object.size();
        cursor = 0;
        //Set the position of the Iterator
        while(cursor < (pos-1))
        {
            next();
        }
    }
    //
    public void add(E elem)
    {//TODO Note sure how to implment waiting for Stefan reply
        Node newNode = new Node(elem, null);
        if(previousNode != null) {
            previousNode.setRight(newNode);
            previousNode = newNode;
        } else if (previousNode == null) {
            object.startNode = newNode;
            previousNode = newNode;
            previousNode.setRight(currentNode);
        }
            
        if(currentNode != null) {
            previousNode.setRight(currentNode);
        } else if (currentNode == null) {
            object.endNode = previousNode;
        }
        cursor++;
        size++;
        object.size++;
    }
    //
    public boolean hasNext()
    {
        return cursor != size;
    }
    //
    public boolean hasPrevious()
    {
        return cursor != 0;
    }
    //
    public E next()
    {
        if(cursor == size)
            throw new NoSuchElementException();
        previousNode = currentNode;
        currentNode = currentNode.getRight();
        cursor++;
        return previousNode.getLeft();
    }
    //
    public int nextIndex()
    {
        return cursor;
    }
    //
    public E previous()
    {
        if(cursor == 0) 
            throw new NoSuchElementException();
        Node<E> node = object.startNode;
        previousNode = null;
        for(int i = 0; i < previousIndex(); i++)
        {
            if((i < previousIndex()) && (size != 0)) { previousNode = node; }
            node = node.getRight();
        }
        currentNode = node;
        cursor--;
        return node.getLeft();
    }
    //
    public int previousIndex()
    {
        return cursor - 1;
    }
    
    public void set(E e) { throw new UnsupportedOperationException(); }
    public void remove() { throw new UnsupportedOperationException(); }
}
