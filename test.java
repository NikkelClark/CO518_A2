import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Write a description of class test here.
 *
 * @author (your name)
 * @version (a version number or a date)
 * 
 * 
 * I made this class to test the object however I would recommend making your own test object
 */
public class test
{
    //* instance variables - replace the example below with your own
    Collection<Integer> array = new ArrayList<Integer>();
    SinglyLinkedList<Integer> SLL;
    SinglyListIterator<Integer> a;
    Object e;
    
    public void itrNext()
    {
        e = a.next();
    }
    
    public void itrPrevious()
    {
        e = a.previous();
    }
    
    public void itrAdd(int elem)
    {
        a.add(elem);
    }
    
    public void itrHasNext()
    {
        System.out.println(a.hasNext());
    }
    
    public void itrHasPrevious()
    {
        System.out.println(a.hasPrevious());
    }
    
    public void itrTest()
    {
        while(a.hasNext())
            System.out.println("- "+a.next());
    }
    
    public void sizeTest()
    {
        System.out.println(SLL.size());
    }
    
    public boolean addTest(Integer elem)
    {
        System.out.println(elem);
        return SLL.add(elem);
    }

    
    /**
     * Constructor for objects of class test
     */
    public test()
    {
        array.add(Integer.valueOf(2));
        array.add((Integer) 5);
        array.add((Integer) 7);
        array.add((Integer) 3);
        array.add((Integer) 6);
        
        SLL = new SinglyLinkedList<Integer>(array);
        a = SLL.listIterator(0);
    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public void sampleMethod()
    {
        
    }
}
