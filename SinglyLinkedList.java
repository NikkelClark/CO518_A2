import java.util.AbstractSequentialList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.List;

/**
 * Write a description of class SinglyLinkedList here.
 *
 * @author Ryan Clark
 * @version 26 November 2017
 * Useful links - https://docs.oracle.com/javase/7/docs/api/java/util/AbstractSequentialList.html
 */
public class SinglyLinkedList<E> extends AbstractSequentialList<E>
{
    // instance variables - replace the example below with your own
    protected Node startNode;
    protected Node endNode;
    protected int size;

    /**
     * Constructor for objects of class SinglyLinkedList
     */
    public SinglyLinkedList()
    {
        // initialise instance variables
        Node curNode = new Node(null, null);
        startNode = curNode;
        endNode = curNode;
    }
    
    /**
     * Constructor for objects of class SinglyLinkedList
     * @param collection    list of elements
     */
    public SinglyLinkedList(Collection<E> collection)
    {
        startNode = new Node(null, null);
        endNode = startNode;
        
        Iterator colIterator = collection.iterator();
        while(colIterator.hasNext())
            add((E)colIterator.next());
    }
    
    /**
     * ListIterator that returns an iterator of the values
     * @param   pos     Position to start in a iterated list
     * @return          Iterator with all the values within the list
     */
    public SinglyListIterator<E> listIterator(int pos)
    { 
        return new SinglyListIterator(pos, this);
    }
    
    /**
     * size returns the size of the linked list
     * @return  The size of the linkedlist
     */
    public int size()
    {
        return size;
    }
    
    /**
     * Add an element into the list
     * @return boolean true
     */
    public boolean add(E elem)
    {
        Node curNode = endNode;
        Node newNode = new Node(null, null);
        if(size == 0)
        {
            curNode.setLeft(elem);
            curNode.setRight(null);
            endNode = curNode;
        } else
        {
            newNode.setLeft(elem);
            curNode.setRight(newNode);
            endNode = newNode;
        }
        size++;
        return true;
    }
}
